//
//  signUp.swift
//  Taqeem.1
//
//  Created by noura tamimi on 04/06/2018.
//  Copyright © 2018 Eng.Hatoun 👩🏻‍💻. All rights reserved.
//

import UIKit

class signUp: UIViewController , UITextFieldDelegate ,UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var picker1: UIPickerView!
    var list = ["الهوية" , "الإقامة"]
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return list.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return list [row]
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBOutlet weak var textfield: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}
