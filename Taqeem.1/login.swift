//
//  logInView.swift
//  Taqeem.1
//
//  Created by noura tamimi on 04/06/2018.
//  Copyright © 2018 Eng.Hatoun 👩🏻‍💻. All rights reserved.
//

import UIKit

class login: UIViewController , UITextFieldDelegate {
    
    @IBOutlet weak var textfield: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}
