//
//  ViewControllerProfileStatic.swift
//  Taqeem.1
//
//  Created by Eng.Hatoun 👩🏻‍💻 on 20/09/1439 AH.
//  Copyright © 1439 Eng.Hatoun 👩🏻‍💻. All rights reserved.
//

import UIKit

class ViewControllerProfileStatic: UIViewController {
   
    override func viewDidLoad() {
        super.viewDidLoad()
        let navController=navigationController!
        let image=#imageLiteral(resourceName: "Screen Shot 1439-09-18 at 3.47.22 AM-1")
        let imageView=UIImageView(image: image)
        let width1=navController.navigationBar.frame.size.width
        let hieght1=navController.navigationBar.frame.size.height
        let bannerx=width1 / 2 - image.size.width / 2
        let bannery=hieght1 / 2 - image.size.height / 2
        
        imageView.frame=CGRect(x: 0, y: 0, width: bannerx, height: bannery)
        imageView.contentMode = .scaleAspectFit
        navigationItem.titleView=imageView
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}
